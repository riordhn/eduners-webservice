<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('signin');
});
Route::get('signin', 'UserController@indexSignIn');
Route::post('signin', 'UserController@actionSignIn');
Route::get('signout', 'UserController@actionSignOut');

Route::group(['middleware' => ['auth']], function () {
    Route::get('welcome', 'UserController@indexWelcome');
    Route::get('password', 'UserController@indexPassword');
    Route::post('password', 'UserController@actionChangePassword');

    Route::group(['prefix' => 'book'], function(){
        Route::get('/', 'BookController@indexList');
        Route::post('table', 'BookController@commonList');

        Route::get('add', 'BookController@indexManage');
        Route::get('edit/{id}', 'BookController@indexManage');
        Route::post('/', 'BookController@actionSave');
    });

    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@indexList');
        Route::post('table', 'UserController@commonList');

        Route::group(['prefix' => 'book'], function(){
            Route::get('detail/{id}', 'UserController@indexBooks');
            Route::post('{id}/table', 'BookController@bookByUserList');
            
            Route::post('add', 'BookController@actionAddUserBook');
            Route::post('delete', 'BookController@actionDeleteUserBook');
        });
    });
});