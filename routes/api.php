<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', function(){
    return 'You haven\'t permission for it';
});
Route::post('v1/signin', 'Api\v1\UserController@signin');
Route::post('v1/register', 'Api\v1\UserController@register');

Route::get('v1/all-books', 'Api\v1\UserController@getAllBooks');

Route::group(array('middleware'=> ['auth.mobile'], 'prefix' => 'v1'), function(){
    Route::get('my-books', 'Api\v1\UserController@getMyBooks');
    Route::post('signout', 'Api\v1\UserController@signout');
});