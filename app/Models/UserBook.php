<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserBook
 */
class UserBook extends Model
{
    protected $table = 'user_books';

    protected $primaryKey = 'user_book_id';

	public $timestamps = false;
    
    protected $fillable = [
        'user_id',
        'book_id',
    ];

    protected $guarded = [];

        
}