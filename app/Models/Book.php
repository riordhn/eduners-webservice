<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Book
 */
class Book extends Model
{
    use SoftDeletes;

    protected $table = 'books';

    protected $primaryKey = 'book_id';

	public $timestamps = true;
    
    protected $fillable = [
        'name',
        'cover_url',
        'is_free',
        'is_local',
        'book_path',
    ];

    protected $guarded = [];

        
}