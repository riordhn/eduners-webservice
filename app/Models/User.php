<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 */
class User extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'users';

    protected $primaryKey = 'user_id';

	public $timestamps = true;
    
    protected $fillable = [
        'email',
        'password',
        'api_id',
        'api_key',
    ];

    protected $guarded = [];

    public function my_books(){
        return $this->hasMany('\App\Models\UserBook', 'user_id')->join('books', 'books.book_id', '=', 'user_books.book_id')->orderBy('books.name', 'asc');
    }
        
}