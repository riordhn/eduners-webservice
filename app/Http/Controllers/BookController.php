<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Yajra\Datatables\Datatables;

use App\Models\Book;
use App\Models\User;
use App\Models\UserBook;

use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Validator;

class BookController extends BaseController{
    public function indexList(Request $request){
        return view('book');
    }

    public function indexManage(Request $request, $id = 0){
        if($item = Book::find($id)){

        }else{
            $item = null;
        }
        return view('book-manage', compact('item'));
    }

    public function commonList(Request $request){
        $input = (object) $request->input();
        $list_data = Book::orderBy('name');

        if(!empty($input->status) && $input->status == 'paid'){
            $list_data = $list_data->where('is_free', 'no');
        }

        if(!empty($input->status) && $input->status == 'free'){
            $list_data = $list_data->where('is_free', 'yes');
        }

        return Datatables::of($list_data)
                ->addColumn('status', function($item){
                    if($item->is_free == 'yes'){
                        return 'Free Book';
                    }else if($item->is_free == 'no'){
                        return 'Paid Book';
                    }
                })
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->book_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function bookByUserList(Request $request, $id = 0){
        if($user = User::find($id)){
            $list_data = $user->my_books;
        }else{
            $list_data = array();
        }

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->book_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSave(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'is_free' => 'required',
            'is_local' => 'required',
            'book_path' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $input = (object) $request->input();

        $redirect = false;
        DB::transaction(function () use ($input, $redirect) {
            if($item = Book::find($input->id)){
                $item->name = $input->name;
                $item->cover_url = $input->cover_url;
                $item->is_free = $input->is_free;
                $item->is_local = $input->is_local;
                $item->book_path = $input->book_path;

                $item->save();
                $redirect = false;
            }else{
                $item = new Book;
                $item->name = $input->name;
                $item->cover_url = $input->cover_url;
                $item->is_free = $input->is_free;
                $item->is_local = $input->is_local;
                $item->book_path = $input->book_path;

                $item->save();
                $redirect = true;
            }
        }, 3);

        if($redirect){
            return redirect('book')->with('toast', 'Item save successfully');
        }else{
            return back();
        }
    }

    public function actionAddUserBook(Request $request){
        $input = (object) $request->input();

        DB::transaction(function () use ($input) {
            if($item = UserBook::where(['user_id' => $input->user_id, 'book_id' => $input->book_id])->first()){
    
            }else{
                $item = new UserBook;
                $item->user_id = $input->user_id;
                $item->book_id = $input->book_id;

                $item->save();
            }
        }, 3);

        return 200;
    }

    public function actionDeleteUserBook(Request $request){
        $input = (object) $request->input();
        if($item = UserBook::where(['user_id' => $input->user_id, 'book_id' => $input->book_id])->first()){
            $item->delete();
        }   
        return 200;
    }
}