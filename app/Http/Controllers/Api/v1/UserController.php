<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use App\Models\Book;
use App\Models\User;

use DB;
use Validator;

class UserController extends BaseController{
    public function signin(Request $request){
        $input = (object) $request->input();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        if($user = User::where(['email' => $input->email, 'role' => 'user'])->first()){
            if(Hash::check($input->password, $user->password)) {
                $user->api_key = hash('ripemd128', uniqid());
                $user->save();
                return response()->json([
                    'status_code' 	=> 200,
                    'status_text' 	=> 'Success',
                    'message' => 'Berhasil login',
                    'user' => $user->only('email', 'api_id', 'api_key')
                ]);
            }else{
                return response()->json([
                    'status_code' 	=> 201,
                    'status_text' 	=> 'Failed',
                    'message' => 'Password akun anda salah'
                ]);
            }
        }else{
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => 'Akun tidak ditemukan'
            ]);
        }
    }

    public function signout(Request $request){
        $input = (object) $request->input();
        $user = $input->auth_data->user;
        
        $user->api_key      = null;
        $user->save();

        return response()->json([
            'status_code' 	=> 200,
            'status_text' 	=> 'Success',
            'message' => ''
        ]);
    }
    
    public function register(){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => (env('APP_DEBUG', 'true') == 'true')? $validator->errors()->messages() : 'Operation error'
            ]);
        }

        if($user = User::where(['email' => $input->email, 'role' => 'user'])->first()){
            return response()->json([
                'status_code' 	=> 201,
                'status_text' 	=> 'Failed',
                'message' => 'Email sudah digunakan, silakan login'
            ]);
        }else{
            $user              = new User;
            $user->email       = $input->email;
            $user->password    = Hash::make($input->password);
            $user->api_id      = hash('ripemd128', uniqid());
            $user->save();
    
            return response()->json([
                'status_code' 	=> 200,
                'status_text' 	=> 'Success',
                'message' => 'Akun berhasil dibuat'
            ]);
        }
    }

    public function getMyBooks(Request $request){
        $input = (object) $request->input();
        $user = $input->auth_data->user;
    
        $all_book = Book::leftJoin('user_books', function($query) use ($user){
            $query->on('user_books.book_id', '=', 'books.book_id')
            ->where('user_books.user_id', $user->user_id);
        })->get();

        return response()->json([
            'status_code' 	=> 200,
            'status_text' 	=> 'Success',
            'message' => '',
            'all_book' => $all_book
        ]);
    }

    public function getAllBooks(Request $request){
        $input = (object) $request->input();
        
        $all_book = Book::leftJoin('user_books', function($query){
            $query->on('user_books.book_id', '=', 'books.book_id')
            ->where('user_books.user_id', 0);
        })->get();

        return response()->json([
            'status_code' 	=> 200,
            'status_text' 	=> 'Success',
            'message' => '',
            'all_book' => $all_book
        ]);
    }
}
