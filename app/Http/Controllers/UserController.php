<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

use Yajra\Datatables\Datatables;

use App\Models\User;
use App\Models\UserBook;

use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Validator;

class UserController extends BaseController{
    public function indexSignIn(Request $request){
        $input = (object) $request->input();
        if(Auth::check()){
            return redirect('welcome');
        }else{
            return view('signin');
        }
    }

    public function indexWelcome(Request $request){
        return view('welcome');
    }

    public function indexPassword(Request $request){
        return view('password');
    }

    public function indexList(Request $request){
        return view('user');
    }

    public function indexBooks(Request $request, $id = 0){
        if($item = User::find($id)){
            return view('user-books', compact('item'));
        }else{
            return abort(404);
        }
    }

    public function commonList(Request $request){
        $list_data = User::where('role', 'user')->orderBy('email');

        return Datatables::of($list_data)
                ->addColumn('action', function($item){
                    $data = array(
                        'id' => $item->user_id
                    );
                    return $data;
                })
                ->make(true);
    }

    public function actionSignIn(Request $request){
        $input = (object) $request->input();
        if ($user = User::where(['email' => $input->email, 'role' => 'admin'])->first()) {
            if (Hash::check($input->password, $user->password)) {
                Auth::loginUsingId($user->user_id);
                return redirect('welcome');
            }else{
                return back()->with('toast', 'Invalid your password');
            }
        }else{
            return back()->with('toast', 'Sign in failed');
        }
    }

    public function actionSignOut(Request $request){
        Auth::logout();
        Session::flush();
        return redirect('/')->with('Sign out successfully');
    }

    public function actionChangePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'new_confirm_password' => 'required'
        ]);

        if($validator->fails()) {
            return back()->with('toast', $validator->errors()->first());
        }

        $user = Auth::user();
        $input = (object) $request->input();
        if($input->new_password == $input->new_confirm_password){
            if (Auth::once(['email' => $user->email, 'password' => $input->old_password])) {
                $user->password = Hash::make($input->new_password);
                $user->save();
                return back()->with('toast', 'Change password successfully');
            }else{
                return back()->with('toast', 'Your old password is incorrect');
            }
        }else{
            return back()->with('toast', 'Re-type your new password again');
        }
    }
}