<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class MyMobileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_id = $request->input('api_id');
        $api_key = $request->input('api_key');

        if($user = User::where(['api_id' => $api_id, 'api_key' => $api_key])->first()){
            $auth_data = (object) array(
                'user' => $user
            );
            $request->request->add(['auth_data' => $auth_data]);
            
            return $next($request);
        }else{
            return response()->json([
                'status_code' 	=> 300,
                'status_text' 	=> 'Logout',
                'message' 	=> 'Authentification failed'
            ]);
        }
    }
}