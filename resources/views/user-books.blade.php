@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
    @include('partials/topbar')
    @include('partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DATA BUKU AKUN {{$item->email}}
                            </h2>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#mybooks" data-toggle="tab" aria-expanded="true">Buku Berbayar Saya</a></li>
                                <li role="presentation" class=""><a href="#selected" data-toggle="tab" aria-expanded="false">Semua Buku Berbayar</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="mybooks">
                                    <div class="table-responsive">
                                        <table id="primary_table" class="table table-bordered table-striped table-hover dataTable display responsive nowrap" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="selected">
                                    <div class="table-responsive">
                                        <table id="secondary_table" class="table table-bordered table-striped table-hover dataTable display responsive nowrap" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });

    var asset = '{{asset('/')}}';
    var base_url = '{{url('/')}}';

    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('user/book/'.$item->user_id.'/table')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'cover_url', searchable: false, orderable: false,
                render: function(data){
                    if(data !== null){
                        return '<img style="width: 75px;" src='+ data +'>';
                    }else{
                        return '<img style="width: 75px;" src="{{asset('media/image-not-found.jpg')}}">';
                    }
                }
            },
            { data: 'name' },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a type="button" class="btn bg-pink btn-circle waves-effect waves-circle waves-float" onclick="actionDelete(this)" data-id="' + data.id +'">'+
                            '    <i class="material-icons">delete</i>'+
                            '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    var secondary_table = $('#secondary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('book/table?status=paid')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'cover_url', searchable: false, orderable: false,
                render: function(data){
                    if(data !== null){
                        return '<img style="width: 75px;" src='+ data +'>';
                    }else{
                        return '<img style="width: 75px;" src="{{asset('media/image-not-found.jpg')}}">';
                    }
                }
            },
            { data: 'name' },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a type="button" class="btn bg-primary btn-circle waves-effect waves-circle waves-float" onclick="actionAdd(this)" data-id="' + data.id +'">'+
                            '    <i class="material-icons">add_circle_outline</i>'+
                            '</a>';
                }
            }
        ]
    });

    secondary_table.on( 'draw', function () {
        secondary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();

    function actionAdd(element){
        var item = $(element);
        item.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: '{{url('user/book/add')}}',
            data:{
                user_id: {{$item->user_id}},
                book_id: item.attr('data-id')
            },
            success: function (data) {
                primary_table.ajax.reload(null, false);
                secondary_table.ajax.reload(null, false);
            },
            completed: function (data){
                item.prop('disabled', false);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    function actionDelete(element){
        var item = $(element);
        item.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: '{{url('user/book/delete')}}',
            data:{
                user_id: {{$item->user_id}},
                book_id: item.attr('data-id')
            },
            success: function (data) {
                primary_table.ajax.reload(null, false);
                secondary_table.ajax.reload(null, false);
            },
            completed: function (data){
                item.prop('disabled', false);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
</script>
@endsection
