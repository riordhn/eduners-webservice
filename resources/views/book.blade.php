@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
    @include('partials/topbar')
    @include('partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue-grey">
                            <h2>
                                DATA BUKU
                            </h2>
                            <div class="header-dropdown m-r-15" style="top:12px">
                                <a class="btn bg-green waves-effect" href="{{url('book/add')}}">
                                    <i class="material-icons">add_circle_outline</i>
                                    <span>Tambah</span>
                                </a>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="primary_table" class="table table-bordered table-striped table-hover dataTable display responsive nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });

    var base_url = '{{url('/')}}';

    var primary_table = $('#primary_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{url('book/table')}}',
            type: 'POST'
        },
        columns: [
            { data: null, searchable: false, orderable: false },
            { data: 'cover_url', searchable: false, orderable: false,
                render: function(data){
                    if(data !== null){
                        return '<img style="width: 75px;" src='+ data +'>';
                    }else{
                        return '<img style="width: 75px;" src="{{asset('media/image-not-found.jpg')}}">';
                    }
                }
            },
            { data: 'name' },
            { data: 'status', searchable: false },
            { data: 'action', searchable: false, orderable: false,
                render: function(data) {
                    return '<a type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float" href="'+ base_url + '/book/edit/' + data.id +'">'+
                            '    <i class="material-icons">mode_edit</i>'+
                            '</a>';
                }
            }
        ]
    });

    primary_table.on( 'draw', function () {
        primary_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            var start = this.page.info().page * 10;
            cell.innerHTML = start + i + 1;
        } );
    } ).draw();
</script>
@endsection
