@php
    $user = Auth::user();
@endphp
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">{{$user->email}}<br>
                <a class="font-bold col-pink font-underline" style="display: contents;" href="{{url('password')}}">Password</a> | 
                <a class="font-bold col-pink font-underline" style="display: contents;" href="{{url('signout')}}">Logout</a></li>
                <li class="active">
                    <a href="{{url('welcome')}}">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('book')}}">
                        <i class="material-icons">book</i>
                        <span>Book</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('user')}}">
                        <i class="material-icons">people</i>
                        <span>User</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                Copyright Eduners&copy;2019 
            </div>
            <div class="version">
                Made with <span style="color: #e25555;">&hearts;</span> by @riordhn
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>