@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
    @include('partials/topbar')
    @include('partials/sidebar')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{Carbon\Carbon::now('Asia/Jakarta')->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue-grey">
                            <h2>
                                MANAGE BUKU
                            </h2>
                        </div>
                        <div class="body">
                            <form class="form-validation" method="POST" action="{{url('book')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" @if(!empty($item)) value="{{$item->book_id}}" @endif>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Name</h2>
                                        <input type="text" class="form-control" name="name" required="" aria-required="true" aria-invalid="true" @if(!empty($item)) value="{{$item->name}}" @endif>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Cover URL</h2>
                                        <input type="text" class="form-control" name="cover_url" @if(!empty($item)) value="{{$item->cover_url}}" @endif>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Free</h2>
                                        <select class="form-control show-tick" name="is_free" required="">
                                            @if(!empty($item))
                                                @if($item->is_free == 'yes')
                                                <option value="yes" selected="" >YES</option>
                                                @elseif($item->is_free == 'no')
                                                <option value="no" selected="" >No</option>
                                                @endif
                                            @else
                                            <option value="yes" selected="" >YES</option>
                                            <option value="no" >NO</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Local</h2>
                                        <select class="form-control show-tick" name="is_local" required="">
                                            <option value="yes" @if(!empty($item) && $item->is_local == 'yes') selected="" @endif>YES</option>
                                            <option value="no" @if(!empty($item) && $item->is_local == 'no') selected="" @endif>NO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h2 class="card-inside-title">Path</h2>
                                        <input type="text" class="form-control" name="book_path" required="" aria-required="true" aria-invalid="true" @if(!empty($item)) value="{{$item->book_path}}" @endif>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button class="btn btn-block bg-green waves-effect" type="submit">Save</button>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <a href="{{url('book')}}" class="btn btn-block bg-pink waves-effect">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection