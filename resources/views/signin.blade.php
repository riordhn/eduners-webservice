@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
<body class="login-page" style="background: #607D8B">
    <div class="login-box">
        <div class="card">
            <div class="body">
                <div class="logo align-center">
                    <img class="m-b-15" src="{{asset('media/logo.jpg')}}" width="192"/>
                </div>
                @if(request()->has('u'))
                <form class="form-validation" method="POST" action="{{url('signin?u='.request()->get('u'))}}">
                @else
                <form class="form-validation" method="POST" action="{{url('signin')}}">
                @endif
                    {{csrf_field()}}
                    <div class="msg">Sign in to access your dashboard</div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" name="email" required="" aria-required="true" aria-invalid="true">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" required="" aria-required="true">
                                    <label class="form-label">Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">LOGIN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
@endsection
@section('js')
<!-- Javascript -->
@endsection