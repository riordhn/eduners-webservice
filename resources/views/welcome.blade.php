@extends('app')
@section('meta')
<!-- Meta -->
@endsection
@section('content')
    @include('partials/topbar')
    @include('partials/sidebar')
    @php
        $today = Carbon\Carbon::today('Asia/Jakarta');
        $yesterday = Carbon\Carbon::yesterday('Asia/Jakarta');
    @endphp
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD | {{$today->format('d M Y')}}</h2>
            </div>
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header bg-blue-grey">
                            <h4><b>Welcome</b></h4>
                        </div>
                        <div class="body">
                        Selamat datang di Admin dashboard website Anda</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
@endsection
@section('js')
@endsection
